Read in:
Process:
  Read in data
  Save md5sum of data
  save a chunk number
  Compress data to where compressed size is ~4.5-5mb
  write to image in this order:
    [cunkNo, m5d, DATA]

Images2File:
  Input zip of files
  read each image, throw all the data into an array
  sort the array by the chunkNo section
  loop through, decompress data check that the md5 matches the md5 of the decomp data
  add the decomp data to a byte array
  write the final bytearray to file when you've reached the end of chunks
